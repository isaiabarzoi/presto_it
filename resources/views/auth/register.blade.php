<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">Registrati</h1>
                <form action="{{route('register')}}" method="POST">
                    @csrf
                    <!-- Name -->
                    <div class="form-outline mb-4">
                        <input name="name" type="text" id="name" class="form-control" aria-describedby="name"/>
                        <label class="form-label" for="name">Nome</label>
                    </div>
                  
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                      <input name="email" type="email" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp"/>
                      <label class="form-label" for="exampleInputEmail1">Email</label>
                    </div>
                  
                    <!-- Password input -->
                    <div class="form-outline mb-4">
                      <input name="password" type="password" id="exampleInputPassword1" class="form-control"/>
                      <label class="form-label" for="exampleInputPassword1">Password</label>
                    </div>
            
                    <!-- Password Confirmation -->
                    <div class="form-outline mb-4">
                        <input name="password_confirmation" type="password" id="password_confirmation" class="form-control"/>
                        <label class="form-label" for="password_confirmation">Conferma Password</label>
                    </div>
            
                    {{-- <!-- Checkbox -->
                    <div class="form-check d-flex justify-content-center mb-4">
                      <input class="form-check-input me-2" type="checkbox" value="" id="form2Example33" checked />
                      <label class="form-check-label" for="form2Example33">
                        Subscribe to our newsletter
                      </label>
                    </div> --}}
                  
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block mb-4">Registrati</button>
                  
                    <!-- Register buttons -->
                    <div class="text-center">
                      <p>oppure registrati con:</p>
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-facebook-f"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-google"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-twitter"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-github"></i>
                      </button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
    
</x-layout>