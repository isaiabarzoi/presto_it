<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-center">Accedi</h1>
                <form action="{{route('login')}}" method="POST">
                    @csrf
                    <!-- Email input -->
                    <div class="form-outline mb-4">
                        <input name="email" type="email" id="exampleInputEmail1" class="form-control" aria-describedby="emailHelp"/>
                        <label class="form-label" for="exampleInputEmail1">Email</label>
                    </div>
                  
                    <!-- Password input -->
                    <div class="form-outline mb-4">
                        <input name="password" type="password" id="exampleInputPassword1" class="form-control"/>
                        <label class="form-label" for="exampleInputPassword1">Password</label>
                    </div>
                  
                    <!-- 2 column grid layout for inline styling -->
                    <div class="row mb-4">
                      <div class="col d-flex justify-content-center">
                        <!-- Checkbox -->
                        <div class="form-check">
                          <input  name="remember" class="form-check-input" type="checkbox" value="" id="exampleCheck1" checked />
                          <label class="form-check-label" for="exampleCheck1">Ricordati di me</label>
                        </div>
                      </div>
                  
                      <div class="col">
                        <!-- Simple link -->
                        <a href="#!">Hai dimenticato la password?</a>
                      </div>
                    </div>
                  
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block mb-4">Accedi</button>
                  
                    <!-- Register buttons -->
                    <div class="text-center">
                      <p>Non sei un membro? <a href="#!">Registrati</a></p>
                      <p>o accedi con:</p>
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-facebook-f"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-google"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-twitter"></i>
                      </button>
                  
                      <button type="button" class="btn btn-secondary btn-floating mx-1">
                        <i class="fab fa-github"></i>
                      </button>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</x-layout>